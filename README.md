# Terraform Module Registry
This project is an Private Module Registry for use with Terraform. This code
implements the
[Terraform Registry API](https://www.terraform.io/docs/registry/api.html).

[Terraform](https://www.terraform.io/) is an infrastructure-as-code tool made by
[Hashicorp](https://www.hashicorp.com/).

## Building
Standard docker build rules

```bash
$ docker build -t tfregistry:<VERSION> .
```

## Configuration
None required

## Running
Execute the docker container

```bash
$ docker run --rm -v /path/to/db:/srv/database -p 8000:8000 tfregistry:<VERSION>
```

There is currently not a ui.
