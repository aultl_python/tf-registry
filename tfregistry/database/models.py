# The examples in this file come from the Flask-SQLAlchemy documentation
# For more information take a look at:
# http://flask-sqlalchemy.pocoo.org/2.1/quickstart/#simple-relationships

from datetime import datetime

from rest_api_demo.database import db

class Module(db.Model):
    id = db.Column(db.String, primary_key=True)
    owner = db.Column(db.String(50))
    namespace = db.Column(db.String(50))
    name = db.Column(db.String(50))
    version = db.Column(db.String(10))
    provider = db.Column(db.String(50))
    description = db.Column(db.Blob())
    source = db.Column(db.String(128))
    publish_time = db.Column(db.DateTime())
    downloads = db.Column(db.Integer())
    verified = db.Column(db.Boolean())

    def __init__(self, id):
        self.id = id

    def __repr__(self):
    return '<Module %r>' % self.id
