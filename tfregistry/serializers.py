from flask_restplus import fields
from tfregistry.restplus import api

module = api.model('Terraform Module', {
    "id": fields.String(description='The module id. format "namespace/name/provider/version'),
    "owner": fields.String(description='The modules maintainer'),
    "namespace": fields.String(description='The module organization'),
    "name": fields.String(description='The module name'),
    "version": fields.String(description='The module symantic version'),
    "provider": fields.String(description='The module author'),
    "description": fields.String(description='Module description'),
    "source": fields.String(description='URL to the source code repository'),
    "published_at": fields.DateTime(description='The date the module was added'),
    "downloads": fields.Integer(description='The cumulative number of downloads'),
    "verified": fields.Boolean(description='Whether the module has been verified'),
})
